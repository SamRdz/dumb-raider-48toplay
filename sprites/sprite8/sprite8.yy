{
    "id": "638a2c97-4380-45fe-aff1-f2de13f640df",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite8",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2bc505e7-eeb1-45ea-bca0-c5d423eb2a7c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "638a2c97-4380-45fe-aff1-f2de13f640df",
            "compositeImage": {
                "id": "33788317-56ec-4fd2-879c-b7dd4be8e06d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2bc505e7-eeb1-45ea-bca0-c5d423eb2a7c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d77c83ce-fe2f-4cfd-84ae-3d46a5d9eaee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2bc505e7-eeb1-45ea-bca0-c5d423eb2a7c",
                    "LayerId": "37791259-219f-454f-8493-cb037febb743"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "37791259-219f-454f-8493-cb037febb743",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "638a2c97-4380-45fe-aff1-f2de13f640df",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}