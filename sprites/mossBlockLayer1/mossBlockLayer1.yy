{
    "id": "991b1b91-4ddd-4fc5-836a-08a00601ba16",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "mossBlockLayer1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b7c377c3-d414-4b44-8f37-d1f00ec1436a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "991b1b91-4ddd-4fc5-836a-08a00601ba16",
            "compositeImage": {
                "id": "f37b549b-ea6c-4f7b-be14-122009df9f34",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b7c377c3-d414-4b44-8f37-d1f00ec1436a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3b5db2a3-ff4d-4279-84ba-f41e0a9bfb23",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b7c377c3-d414-4b44-8f37-d1f00ec1436a",
                    "LayerId": "03184599-fd1a-4638-b56c-a71aa72cec69"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "03184599-fd1a-4638-b56c-a71aa72cec69",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "991b1b91-4ddd-4fc5-836a-08a00601ba16",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}