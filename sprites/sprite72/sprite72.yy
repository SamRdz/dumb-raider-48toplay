{
    "id": "8f6fde32-1783-4376-9626-f12999a15f09",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite72",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2107f0ea-fbc7-4da4-856e-23cefc8fad3d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8f6fde32-1783-4376-9626-f12999a15f09",
            "compositeImage": {
                "id": "2ea4e22a-9f62-4ddc-9ba4-970e0fa8abf3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2107f0ea-fbc7-4da4-856e-23cefc8fad3d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2e55a11f-d95d-4ddc-ad63-ea51bd558f54",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2107f0ea-fbc7-4da4-856e-23cefc8fad3d",
                    "LayerId": "cf3f8f96-3f1c-44d6-b72b-02b2cf7afa95"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "cf3f8f96-3f1c-44d6-b72b-02b2cf7afa95",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8f6fde32-1783-4376-9626-f12999a15f09",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}