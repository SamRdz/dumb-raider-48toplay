{
    "id": "272a335a-2f1d-4cd9-b918-4aa05784259c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "empty",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e0d95a6e-f4c1-4df7-b9bf-4b940a32780a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "272a335a-2f1d-4cd9-b918-4aa05784259c",
            "compositeImage": {
                "id": "c24a607c-961b-4b71-9926-72bbf7c5133f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e0d95a6e-f4c1-4df7-b9bf-4b940a32780a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "491cfbc4-b25c-4818-9ce0-434e594bb0cc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e0d95a6e-f4c1-4df7-b9bf-4b940a32780a",
                    "LayerId": "610d888c-1805-49d5-88ff-35c03428a0a0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "610d888c-1805-49d5-88ff-35c03428a0a0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "272a335a-2f1d-4cd9-b918-4aa05784259c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}