{
    "id": "fa815f55-fba0-4ff3-9784-b2207245c507",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "trueBackground",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1079,
    "bbox_left": 0,
    "bbox_right": 1919,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6b62f8ff-feef-470d-8cad-9d23e3372e3f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fa815f55-fba0-4ff3-9784-b2207245c507",
            "compositeImage": {
                "id": "aa4e2eb8-8833-4d12-a572-14a0bd3c0959",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6b62f8ff-feef-470d-8cad-9d23e3372e3f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "16d21444-5c85-4795-ab15-d5b4a6b1d780",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6b62f8ff-feef-470d-8cad-9d23e3372e3f",
                    "LayerId": "12cbeea0-a1aa-4726-9c65-7ceece5c69dc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1080,
    "layers": [
        {
            "id": "12cbeea0-a1aa-4726-9c65-7ceece5c69dc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fa815f55-fba0-4ff3-9784-b2207245c507",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1920,
    "xorig": 0,
    "yorig": 0
}