{
    "id": "026b55cf-3cd8-4b34-b94e-212a05046481",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "crackedBlockV2Layer2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2e889565-172b-4ca3-a498-1b30a7dc1d5e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "026b55cf-3cd8-4b34-b94e-212a05046481",
            "compositeImage": {
                "id": "85389e59-f498-4efb-af5b-696307b1b9a6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2e889565-172b-4ca3-a498-1b30a7dc1d5e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "03bb7b21-f905-4979-9327-5f957375e2e8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2e889565-172b-4ca3-a498-1b30a7dc1d5e",
                    "LayerId": "088bb4cb-d38e-4268-bc26-de5d8f89f174"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "088bb4cb-d38e-4268-bc26-de5d8f89f174",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "026b55cf-3cd8-4b34-b94e-212a05046481",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}