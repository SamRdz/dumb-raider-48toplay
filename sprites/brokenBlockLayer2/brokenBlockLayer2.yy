{
    "id": "9d8cbff4-1fed-427b-8461-d99076aca274",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "brokenBlockLayer2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a21310b2-b356-4f30-8c94-be5c4d2cdb85",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9d8cbff4-1fed-427b-8461-d99076aca274",
            "compositeImage": {
                "id": "6768d9bf-5d50-42b6-9cf8-dddf153d096d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a21310b2-b356-4f30-8c94-be5c4d2cdb85",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bd0ece3f-f9bc-499c-90ac-1b7c5f55d2c9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a21310b2-b356-4f30-8c94-be5c4d2cdb85",
                    "LayerId": "048f71c2-0f58-4c0e-ae9c-03cadf7fa87f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "048f71c2-0f58-4c0e-ae9c-03cadf7fa87f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9d8cbff4-1fed-427b-8461-d99076aca274",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}