{
    "id": "138a0b7e-be21-45f8-acb1-4a9517bf31a0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite7",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0a4d27d2-2a19-494e-92bc-42a6dc36498c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "138a0b7e-be21-45f8-acb1-4a9517bf31a0",
            "compositeImage": {
                "id": "22137648-3d37-4d43-823f-2930f21fbb8d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0a4d27d2-2a19-494e-92bc-42a6dc36498c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a765b588-ed34-46cd-8784-638071d141a2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0a4d27d2-2a19-494e-92bc-42a6dc36498c",
                    "LayerId": "ae09bf69-bce4-4803-9746-c03238891774"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "ae09bf69-bce4-4803-9746-c03238891774",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "138a0b7e-be21-45f8-acb1-4a9517bf31a0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}