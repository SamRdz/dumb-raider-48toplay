{
    "id": "76678e40-22c5-4802-8463-5a96e101e948",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "mossLayer2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8fff08a5-e059-45d8-ae08-2f3c8936d80b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "76678e40-22c5-4802-8463-5a96e101e948",
            "compositeImage": {
                "id": "577e6481-3651-4b59-95cb-4b0fc01a48bb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8fff08a5-e059-45d8-ae08-2f3c8936d80b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f0c0857f-d71a-4e1a-9fe8-13efb4544c5a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8fff08a5-e059-45d8-ae08-2f3c8936d80b",
                    "LayerId": "580fa231-68a0-41a9-8ca2-8b11f009af83"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "580fa231-68a0-41a9-8ca2-8b11f009af83",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "76678e40-22c5-4802-8463-5a96e101e948",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}