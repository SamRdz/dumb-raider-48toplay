{
    "id": "c7e2040c-736b-4d09-a06c-ea4dbfd746ff",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite81",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5566ffec-f26c-4b8b-9b1e-604f2ee66046",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c7e2040c-736b-4d09-a06c-ea4dbfd746ff",
            "compositeImage": {
                "id": "f1cf755b-6a58-4d3c-a874-7dd87059a453",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5566ffec-f26c-4b8b-9b1e-604f2ee66046",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "39967e5b-b169-474f-b812-c7d7c95ab8cb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5566ffec-f26c-4b8b-9b1e-604f2ee66046",
                    "LayerId": "dee044e4-fbe2-4e06-a1d0-1a96c6063da6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "dee044e4-fbe2-4e06-a1d0-1a96c6063da6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c7e2040c-736b-4d09-a06c-ea4dbfd746ff",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}