{
    "id": "4e20c278-c2bd-44a8-9ea8-c843d9fbee75",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite10",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bc360e2e-9ca7-4e95-9bff-cd52696cb27e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e20c278-c2bd-44a8-9ea8-c843d9fbee75",
            "compositeImage": {
                "id": "b3470190-1b68-406f-93e3-bacb2e6b4d62",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bc360e2e-9ca7-4e95-9bff-cd52696cb27e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c1653a13-0b29-4edd-86d8-8dbb10684879",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bc360e2e-9ca7-4e95-9bff-cd52696cb27e",
                    "LayerId": "d35e1b00-d46d-423e-87ce-085c221d28d5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "d35e1b00-d46d-423e-87ce-085c221d28d5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4e20c278-c2bd-44a8-9ea8-c843d9fbee75",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}