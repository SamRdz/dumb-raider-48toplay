{
    "id": "bb5cc102-88d9-4cfb-b735-7ec6ac9097ba",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "arrowSprite",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 27,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e33161e2-2c0c-4a9f-99f8-5d238837dbee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bb5cc102-88d9-4cfb-b735-7ec6ac9097ba",
            "compositeImage": {
                "id": "7581b9c5-cb15-4ba3-aeaf-b784c14c5ab6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e33161e2-2c0c-4a9f-99f8-5d238837dbee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "43dc8291-4097-49d4-80d5-51291d186007",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e33161e2-2c0c-4a9f-99f8-5d238837dbee",
                    "LayerId": "829aa462-11b0-4d9d-9b90-9264c289ec0f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "829aa462-11b0-4d9d-9b90-9264c289ec0f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bb5cc102-88d9-4cfb-b735-7ec6ac9097ba",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 2,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 63,
    "yorig": 0
}