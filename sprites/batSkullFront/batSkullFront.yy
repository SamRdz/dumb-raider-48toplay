{
    "id": "3611e1ef-535a-4b28-8b9b-2b55656c1ba8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "batSkullFront",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6f4c53a2-8bba-4e79-a9bc-d3659ab3f2fc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3611e1ef-535a-4b28-8b9b-2b55656c1ba8",
            "compositeImage": {
                "id": "e13f391f-21d4-446b-b9c8-29effb100b0e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6f4c53a2-8bba-4e79-a9bc-d3659ab3f2fc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "11c58e5c-ed26-433b-9502-ba9b14bd255e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6f4c53a2-8bba-4e79-a9bc-d3659ab3f2fc",
                    "LayerId": "1bbf3e27-a0d3-411d-ad28-24806c4854c4"
                }
            ]
        },
        {
            "id": "97531127-d4e8-4086-9040-b845c8bc96c9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3611e1ef-535a-4b28-8b9b-2b55656c1ba8",
            "compositeImage": {
                "id": "63ce0d5f-4462-454d-8c12-97eff87523ad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "97531127-d4e8-4086-9040-b845c8bc96c9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3dc3f3f2-2f90-42ab-b1fd-d1fcda71de1c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "97531127-d4e8-4086-9040-b845c8bc96c9",
                    "LayerId": "1bbf3e27-a0d3-411d-ad28-24806c4854c4"
                }
            ]
        },
        {
            "id": "20976889-1b11-4963-8874-7b48cc329084",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3611e1ef-535a-4b28-8b9b-2b55656c1ba8",
            "compositeImage": {
                "id": "8d9f6523-240a-41c0-9504-2a35440f77b2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "20976889-1b11-4963-8874-7b48cc329084",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d71d5d0d-1ca4-4c5d-bbe7-9572aad20642",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "20976889-1b11-4963-8874-7b48cc329084",
                    "LayerId": "1bbf3e27-a0d3-411d-ad28-24806c4854c4"
                }
            ]
        },
        {
            "id": "1e913927-690c-4ae0-b0fd-ac7899c54c60",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3611e1ef-535a-4b28-8b9b-2b55656c1ba8",
            "compositeImage": {
                "id": "fc9c5961-d8fc-4a31-b2b4-18e2dcbebbec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1e913927-690c-4ae0-b0fd-ac7899c54c60",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cf8be7d6-9770-4d90-9ca5-fdd48912b6fd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1e913927-690c-4ae0-b0fd-ac7899c54c60",
                    "LayerId": "1bbf3e27-a0d3-411d-ad28-24806c4854c4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "1bbf3e27-a0d3-411d-ad28-24806c4854c4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3611e1ef-535a-4b28-8b9b-2b55656c1ba8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 8,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}