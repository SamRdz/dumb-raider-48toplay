{
    "id": "eb0d4d2e-51e3-4e76-8f9e-b3be0ff416fe",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "pressurePlateNotPressed",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 54,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "29b088b9-a974-46c5-9711-ad4e28f6a7a0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eb0d4d2e-51e3-4e76-8f9e-b3be0ff416fe",
            "compositeImage": {
                "id": "aef0e70f-22b9-4273-a2bb-fca918b059be",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "29b088b9-a974-46c5-9711-ad4e28f6a7a0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7b44ae81-959c-4c3b-8a62-a88dcc2f9c1f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "29b088b9-a974-46c5-9711-ad4e28f6a7a0",
                    "LayerId": "2dcb2011-de12-4289-b2ad-d969903b4ac1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "2dcb2011-de12-4289-b2ad-d969903b4ac1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "eb0d4d2e-51e3-4e76-8f9e-b3be0ff416fe",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}