{
    "id": "24a39e8d-64f6-428f-8e6f-e62f41152ac8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "blockLayer2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8e1a5fbf-e7d1-442d-bd14-d8f93d439203",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "24a39e8d-64f6-428f-8e6f-e62f41152ac8",
            "compositeImage": {
                "id": "8e909e46-b448-49ce-8c87-edc78d12a9ee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8e1a5fbf-e7d1-442d-bd14-d8f93d439203",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "330d7ab2-869a-4e84-ab72-b19a32372649",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8e1a5fbf-e7d1-442d-bd14-d8f93d439203",
                    "LayerId": "1c027541-6011-434b-8578-4217c4afcbd1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "1c027541-6011-434b-8578-4217c4afcbd1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "24a39e8d-64f6-428f-8e6f-e62f41152ac8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}