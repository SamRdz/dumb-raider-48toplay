{
    "id": "97a68583-4537-4306-a31c-73892614e3aa",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "pillar",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 4,
    "bbox_right": 59,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "37ed1895-eb9b-4c1d-a619-ecd37b0c2cb2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "97a68583-4537-4306-a31c-73892614e3aa",
            "compositeImage": {
                "id": "c1840231-ed89-4cf7-ba0d-b06c4a509bad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "37ed1895-eb9b-4c1d-a619-ecd37b0c2cb2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cb8f9f4d-a475-4973-9513-6606b171f871",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "37ed1895-eb9b-4c1d-a619-ecd37b0c2cb2",
                    "LayerId": "dc2a4ca8-c6bd-4285-b546-875642d668b4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "dc2a4ca8-c6bd-4285-b546-875642d668b4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "97a68583-4537-4306-a31c-73892614e3aa",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}