{
    "id": "74efff2d-a379-4bfa-bab9-3a7cb816ad56",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "faceSensor",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 0,
    "bbox_right": 127,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e7dabad9-be16-49f0-9757-9a96005307ec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "74efff2d-a379-4bfa-bab9-3a7cb816ad56",
            "compositeImage": {
                "id": "ddb81d54-8aad-4141-8a0f-18cc90eb0e11",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e7dabad9-be16-49f0-9757-9a96005307ec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a574da0e-2180-401f-b8d1-c8c881ce0d85",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e7dabad9-be16-49f0-9757-9a96005307ec",
                    "LayerId": "e9a81273-4377-419d-9ac5-267b68bb4f09"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "e9a81273-4377-419d-9ac5-267b68bb4f09",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "74efff2d-a379-4bfa-bab9-3a7cb816ad56",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 0,
    "yorig": 0
}