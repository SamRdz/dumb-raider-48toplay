{
    "id": "31b16921-f43c-4c5a-adc6-66789dc506e8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "snakeMoving",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "92b15e88-d9d7-4e9b-b053-1b0478eaaef0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "31b16921-f43c-4c5a-adc6-66789dc506e8",
            "compositeImage": {
                "id": "4cdb8323-d5f2-4bcf-ade5-5d1640ae6f0c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "92b15e88-d9d7-4e9b-b053-1b0478eaaef0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4625a4e7-4de7-490d-9ac8-20ce09e19b6e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "92b15e88-d9d7-4e9b-b053-1b0478eaaef0",
                    "LayerId": "e03d68d3-a36b-43fb-9027-65da12332300"
                }
            ]
        },
        {
            "id": "66d9eb3a-81d9-4752-9827-b87b26e547af",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "31b16921-f43c-4c5a-adc6-66789dc506e8",
            "compositeImage": {
                "id": "6163e908-da07-4d84-8aff-f71620aa1ac3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "66d9eb3a-81d9-4752-9827-b87b26e547af",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b8f06bb9-4cbb-44db-9fa9-ef00e71e94a3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "66d9eb3a-81d9-4752-9827-b87b26e547af",
                    "LayerId": "e03d68d3-a36b-43fb-9027-65da12332300"
                }
            ]
        },
        {
            "id": "7ff2ec71-e681-483c-8d59-54ad66b021e9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "31b16921-f43c-4c5a-adc6-66789dc506e8",
            "compositeImage": {
                "id": "8375b5e3-200f-4e02-8b8c-e1961580956f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7ff2ec71-e681-483c-8d59-54ad66b021e9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6e08bba8-5b25-497f-8451-dd7ec1cbe20b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7ff2ec71-e681-483c-8d59-54ad66b021e9",
                    "LayerId": "e03d68d3-a36b-43fb-9027-65da12332300"
                }
            ]
        },
        {
            "id": "bc679d47-1b5d-4d6a-bbe8-90349ec65ad1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "31b16921-f43c-4c5a-adc6-66789dc506e8",
            "compositeImage": {
                "id": "5a7d6ee9-442d-474b-8b88-72b33376a350",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bc679d47-1b5d-4d6a-bbe8-90349ec65ad1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "11889941-d196-48bc-acd9-e7220e06ea33",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bc679d47-1b5d-4d6a-bbe8-90349ec65ad1",
                    "LayerId": "e03d68d3-a36b-43fb-9027-65da12332300"
                }
            ]
        },
        {
            "id": "626a48d7-b2b7-4428-8799-ed9782d352d4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "31b16921-f43c-4c5a-adc6-66789dc506e8",
            "compositeImage": {
                "id": "6499dfa1-7248-4ac0-9abc-7f7dc39a032d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "626a48d7-b2b7-4428-8799-ed9782d352d4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "10074090-2045-410f-bf00-6f142ef5434f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "626a48d7-b2b7-4428-8799-ed9782d352d4",
                    "LayerId": "e03d68d3-a36b-43fb-9027-65da12332300"
                }
            ]
        },
        {
            "id": "49759031-d7a6-4129-83bf-8626c125c6d3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "31b16921-f43c-4c5a-adc6-66789dc506e8",
            "compositeImage": {
                "id": "33a28549-d734-45b0-a46c-d969c1936ef0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "49759031-d7a6-4129-83bf-8626c125c6d3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "79ac89c3-e828-4ef1-8d69-a8b76e523895",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "49759031-d7a6-4129-83bf-8626c125c6d3",
                    "LayerId": "e03d68d3-a36b-43fb-9027-65da12332300"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "e03d68d3-a36b-43fb-9027-65da12332300",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "31b16921-f43c-4c5a-adc6-66789dc506e8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 6,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}