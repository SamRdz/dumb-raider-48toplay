{
    "id": "14da72d5-308c-4fa9-99e9-a28214cfd2aa",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite12",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b40485f3-8e67-4afa-a82c-6f271f343f6e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "14da72d5-308c-4fa9-99e9-a28214cfd2aa",
            "compositeImage": {
                "id": "254bbd73-6ab6-4449-be40-fcf5957a4bc7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b40485f3-8e67-4afa-a82c-6f271f343f6e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d0413492-233d-4d53-8f86-bb44f6edb1c3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b40485f3-8e67-4afa-a82c-6f271f343f6e",
                    "LayerId": "1812e55c-1637-4e83-bcc5-1d2f3a1d9608"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "1812e55c-1637-4e83-bcc5-1d2f3a1d9608",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "14da72d5-308c-4fa9-99e9-a28214cfd2aa",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}