{
    "id": "2199c2c0-85c7-4546-a25b-f23e1430581c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "torchLever",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 12,
    "bbox_right": 19,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a7808b28-80b5-4276-ace8-ef0cc07fd6a1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2199c2c0-85c7-4546-a25b-f23e1430581c",
            "compositeImage": {
                "id": "a0bf745e-ecb7-4a75-9892-30e3d29bc6c3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a7808b28-80b5-4276-ace8-ef0cc07fd6a1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2d7593dd-8187-49f8-af1e-56660dd68b61",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a7808b28-80b5-4276-ace8-ef0cc07fd6a1",
                    "LayerId": "54f8fd9a-3611-4e8c-bbda-aab0fee2456c"
                }
            ]
        },
        {
            "id": "ad0886bc-fc37-4514-98e9-c6e98058e5b0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2199c2c0-85c7-4546-a25b-f23e1430581c",
            "compositeImage": {
                "id": "9f05d5d2-3eb8-470b-af13-43e35f3fcf5a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ad0886bc-fc37-4514-98e9-c6e98058e5b0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f9afff06-9dbc-4673-98be-b75422d0d87d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ad0886bc-fc37-4514-98e9-c6e98058e5b0",
                    "LayerId": "54f8fd9a-3611-4e8c-bbda-aab0fee2456c"
                }
            ]
        },
        {
            "id": "5de988c9-d1da-4684-a46f-fce0f7677298",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2199c2c0-85c7-4546-a25b-f23e1430581c",
            "compositeImage": {
                "id": "2bbcb480-868c-4e87-b065-bf2ade263d04",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5de988c9-d1da-4684-a46f-fce0f7677298",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "86cb9e12-cf98-4a66-86e5-9e0ac60727e0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5de988c9-d1da-4684-a46f-fce0f7677298",
                    "LayerId": "54f8fd9a-3611-4e8c-bbda-aab0fee2456c"
                }
            ]
        },
        {
            "id": "fd36ea16-1e0f-4e33-8f6f-66926f22ec75",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2199c2c0-85c7-4546-a25b-f23e1430581c",
            "compositeImage": {
                "id": "ef5bcf53-d0e9-4f75-b83b-c00b89e26e8b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fd36ea16-1e0f-4e33-8f6f-66926f22ec75",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "856fe689-bd14-4935-b50b-ccb05ba13148",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fd36ea16-1e0f-4e33-8f6f-66926f22ec75",
                    "LayerId": "54f8fd9a-3611-4e8c-bbda-aab0fee2456c"
                }
            ]
        },
        {
            "id": "4f1e65bc-6284-40bd-be53-6b2f317bb997",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2199c2c0-85c7-4546-a25b-f23e1430581c",
            "compositeImage": {
                "id": "5ea0a34d-09f4-4b2a-947e-70585b8f0e96",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4f1e65bc-6284-40bd-be53-6b2f317bb997",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3721cf6f-02e0-443c-873c-403a797643bb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4f1e65bc-6284-40bd-be53-6b2f317bb997",
                    "LayerId": "54f8fd9a-3611-4e8c-bbda-aab0fee2456c"
                }
            ]
        },
        {
            "id": "b9866d70-99cd-49b2-9f27-f7de7058c0b8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2199c2c0-85c7-4546-a25b-f23e1430581c",
            "compositeImage": {
                "id": "3f876e8b-51f9-4a41-83ea-043b83291c19",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b9866d70-99cd-49b2-9f27-f7de7058c0b8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "32b0c019-a5f1-455e-b642-b69bf1da19d4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b9866d70-99cd-49b2-9f27-f7de7058c0b8",
                    "LayerId": "54f8fd9a-3611-4e8c-bbda-aab0fee2456c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "54f8fd9a-3611-4e8c-bbda-aab0fee2456c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2199c2c0-85c7-4546-a25b-f23e1430581c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 15,
    "yorig": 31
}