{
    "id": "ab9831d4-3d06-470e-995b-d1dd80a0e9f7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "smallTest",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b4a77339-04ed-49c0-a9ff-7e411708dc1d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ab9831d4-3d06-470e-995b-d1dd80a0e9f7",
            "compositeImage": {
                "id": "e0c58802-c3a1-4a6d-96b2-f807c24684d6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b4a77339-04ed-49c0-a9ff-7e411708dc1d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e7aade1b-7d85-43f8-9dbf-23236bbdd721",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b4a77339-04ed-49c0-a9ff-7e411708dc1d",
                    "LayerId": "e22de813-f400-4c47-9ba6-b31d620f3da1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "e22de813-f400-4c47-9ba6-b31d620f3da1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ab9831d4-3d06-470e-995b-d1dd80a0e9f7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 2,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 63,
    "yorig": 0
}