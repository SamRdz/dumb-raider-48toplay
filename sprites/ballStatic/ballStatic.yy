{
    "id": "3f93faa0-0c20-4f62-9d96-f9c3b96b817d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "ballStatic",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 0,
    "bbox_right": 127,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1f308470-7880-4f6e-a333-b1dc060ec9c9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3f93faa0-0c20-4f62-9d96-f9c3b96b817d",
            "compositeImage": {
                "id": "102a0778-5ac7-449c-af4e-c438c54377ae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1f308470-7880-4f6e-a333-b1dc060ec9c9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fb5f342f-fbba-49c9-89ba-bed25be591f5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1f308470-7880-4f6e-a333-b1dc060ec9c9",
                    "LayerId": "81cdb920-2b5f-4e62-9325-5d0a667efaf1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "81cdb920-2b5f-4e62-9325-5d0a667efaf1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3f93faa0-0c20-4f62-9d96-f9c3b96b817d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 0,
    "yorig": 0
}