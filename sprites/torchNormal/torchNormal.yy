{
    "id": "303e4a4f-8dfd-42d2-b263-1e0a4a928df7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "torchNormal",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 12,
    "bbox_right": 19,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e866faa2-9fb7-49d5-bad3-2f66115a01e9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "303e4a4f-8dfd-42d2-b263-1e0a4a928df7",
            "compositeImage": {
                "id": "9648ed99-7e10-41f7-b951-7a69ec6e433c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e866faa2-9fb7-49d5-bad3-2f66115a01e9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ae8c5275-c647-4abf-8d5e-7a9d01fc24e1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e866faa2-9fb7-49d5-bad3-2f66115a01e9",
                    "LayerId": "c1f31273-30e9-4328-8adf-237bfe5bbbbf"
                }
            ]
        },
        {
            "id": "9f6ab832-a177-4116-8e4c-466165ac1755",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "303e4a4f-8dfd-42d2-b263-1e0a4a928df7",
            "compositeImage": {
                "id": "1b7085e1-4ac0-4c78-af37-c95754eba505",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9f6ab832-a177-4116-8e4c-466165ac1755",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c77f1e4f-f3db-4ed7-9e7c-09b170f2c11d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9f6ab832-a177-4116-8e4c-466165ac1755",
                    "LayerId": "c1f31273-30e9-4328-8adf-237bfe5bbbbf"
                }
            ]
        },
        {
            "id": "9ba16a90-b5cc-43e6-a152-37acd6fed1f7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "303e4a4f-8dfd-42d2-b263-1e0a4a928df7",
            "compositeImage": {
                "id": "097c240c-7a71-4a60-84e5-e40e752d7057",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9ba16a90-b5cc-43e6-a152-37acd6fed1f7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8cff18ad-d4c2-4ca5-8e7f-d9e5fd5affb7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9ba16a90-b5cc-43e6-a152-37acd6fed1f7",
                    "LayerId": "c1f31273-30e9-4328-8adf-237bfe5bbbbf"
                }
            ]
        },
        {
            "id": "349df248-ed95-4e62-b01e-35714d5050cd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "303e4a4f-8dfd-42d2-b263-1e0a4a928df7",
            "compositeImage": {
                "id": "f67eb19e-16a5-4625-8929-0df24d3c3055",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "349df248-ed95-4e62-b01e-35714d5050cd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6e8d15d4-dbfd-417c-b002-a445fafe11d5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "349df248-ed95-4e62-b01e-35714d5050cd",
                    "LayerId": "c1f31273-30e9-4328-8adf-237bfe5bbbbf"
                }
            ]
        },
        {
            "id": "e2fbd17a-312f-48ca-b58b-6e798c747bca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "303e4a4f-8dfd-42d2-b263-1e0a4a928df7",
            "compositeImage": {
                "id": "a70a73d5-8e9b-4ba4-acb0-0e95e5c92c32",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e2fbd17a-312f-48ca-b58b-6e798c747bca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a72e5090-b8c2-4011-93d6-ff8f0f27c48f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e2fbd17a-312f-48ca-b58b-6e798c747bca",
                    "LayerId": "c1f31273-30e9-4328-8adf-237bfe5bbbbf"
                }
            ]
        },
        {
            "id": "c4378960-309f-40ad-b30c-2001812603b2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "303e4a4f-8dfd-42d2-b263-1e0a4a928df7",
            "compositeImage": {
                "id": "ec600cfa-f8ec-4d81-a365-747d184a550e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c4378960-309f-40ad-b30c-2001812603b2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9ece511a-ad64-4f80-ac0c-a87d1c285bae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c4378960-309f-40ad-b30c-2001812603b2",
                    "LayerId": "c1f31273-30e9-4328-8adf-237bfe5bbbbf"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "c1f31273-30e9-4328-8adf-237bfe5bbbbf",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "303e4a4f-8dfd-42d2-b263-1e0a4a928df7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 8,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 15,
    "yorig": 31
}