{
    "id": "29811d91-d782-4308-96a9-dafdb01b41c7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite11",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 0,
    "bbox_right": 127,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "848e6d4f-466f-4e1f-a84b-170f90c3167c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "29811d91-d782-4308-96a9-dafdb01b41c7",
            "compositeImage": {
                "id": "bce97ccd-bbe1-4cb2-83ab-ce70f2ccc63f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "848e6d4f-466f-4e1f-a84b-170f90c3167c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "57263b28-cdd6-489c-87d9-41ea6fe829ac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "848e6d4f-466f-4e1f-a84b-170f90c3167c",
                    "LayerId": "072372ae-319b-4cd3-8015-edb95c5b0a98"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "072372ae-319b-4cd3-8015-edb95c5b0a98",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "29811d91-d782-4308-96a9-dafdb01b41c7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 0,
    "yorig": 0
}