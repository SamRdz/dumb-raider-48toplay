{
    "id": "9f1d17d1-cf3d-4862-a48a-2153cbf1ac37",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "batRoof",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0f0cd750-255d-44f3-9666-99bba942ae29",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9f1d17d1-cf3d-4862-a48a-2153cbf1ac37",
            "compositeImage": {
                "id": "8a01c8d4-f386-4b3e-b27f-56faa6bda146",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0f0cd750-255d-44f3-9666-99bba942ae29",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d24d320e-d0aa-40a3-a9f0-017b3bcbdffd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0f0cd750-255d-44f3-9666-99bba942ae29",
                    "LayerId": "551bcf67-8935-4aae-a05c-8951815ca625"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "551bcf67-8935-4aae-a05c-8951815ca625",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9f1d17d1-cf3d-4862-a48a-2153cbf1ac37",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}