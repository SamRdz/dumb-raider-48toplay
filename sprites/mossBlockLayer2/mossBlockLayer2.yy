{
    "id": "a61a7a4d-9659-4c70-a3a3-e9972be3af22",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "mossBlockLayer2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0d1279d8-0d89-418b-bba4-7bc37679fe12",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a61a7a4d-9659-4c70-a3a3-e9972be3af22",
            "compositeImage": {
                "id": "5db6ccf9-c63c-4a91-b1e8-265c0475732f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0d1279d8-0d89-418b-bba4-7bc37679fe12",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fa1c1ca3-f26e-4cbf-959a-a2bb01d739b5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0d1279d8-0d89-418b-bba4-7bc37679fe12",
                    "LayerId": "9384d9a3-320d-4d7f-b371-f3e650ab6fc9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "9384d9a3-320d-4d7f-b371-f3e650ab6fc9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a61a7a4d-9659-4c70-a3a3-e9972be3af22",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}