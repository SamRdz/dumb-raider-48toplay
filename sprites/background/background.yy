{
    "id": "235a2be2-3bdb-439a-b4a4-3c9eb92507ac",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "background",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "841b7c50-64c7-40a3-9092-1f541b377cf4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "235a2be2-3bdb-439a-b4a4-3c9eb92507ac",
            "compositeImage": {
                "id": "3dac586d-e8ce-4b02-a8d4-af7899bd9183",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "841b7c50-64c7-40a3-9092-1f541b377cf4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "00858c36-52ec-412c-ad32-ca1cfbf989f5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "841b7c50-64c7-40a3-9092-1f541b377cf4",
                    "LayerId": "23c4db45-dcca-4419-9804-3fce6198688d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "23c4db45-dcca-4419-9804-3fce6198688d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "235a2be2-3bdb-439a-b4a4-3c9eb92507ac",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}