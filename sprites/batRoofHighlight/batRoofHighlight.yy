{
    "id": "ee40027a-dbb8-4750-902a-03210d9c0eca",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "batRoofHighlight",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e31c88e9-aca9-4ba2-8f23-b9b5b2b729e2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee40027a-dbb8-4750-902a-03210d9c0eca",
            "compositeImage": {
                "id": "cda15ac8-9593-47f7-970d-116181145686",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e31c88e9-aca9-4ba2-8f23-b9b5b2b729e2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "91aba028-86aa-4719-9fbd-2f0d17b4de1e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e31c88e9-aca9-4ba2-8f23-b9b5b2b729e2",
                    "LayerId": "b3211e4e-60e5-45c0-b91f-b55443f76884"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "b3211e4e-60e5-45c0-b91f-b55443f76884",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ee40027a-dbb8-4750-902a-03210d9c0eca",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}