{
    "id": "7b14a0b7-c288-4de5-8bc8-3078d10d6945",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "snakeIdle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "076cd4b9-fde2-4841-9e22-c12a14dfc98d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7b14a0b7-c288-4de5-8bc8-3078d10d6945",
            "compositeImage": {
                "id": "a6ecb80d-5c53-4064-9ed4-e9c52f684980",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "076cd4b9-fde2-4841-9e22-c12a14dfc98d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c91dda24-670c-4802-839c-9f783cb64b3c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "076cd4b9-fde2-4841-9e22-c12a14dfc98d",
                    "LayerId": "742fc847-05b4-44a4-9b8c-2d38b5452002"
                }
            ]
        },
        {
            "id": "72f0e598-425e-48e0-8e85-77917fe8bd19",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7b14a0b7-c288-4de5-8bc8-3078d10d6945",
            "compositeImage": {
                "id": "dceb445b-5ac8-435c-b689-51338bf626c9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "72f0e598-425e-48e0-8e85-77917fe8bd19",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ddc29838-f0d1-4348-a6f7-27d7f1dcb235",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "72f0e598-425e-48e0-8e85-77917fe8bd19",
                    "LayerId": "742fc847-05b4-44a4-9b8c-2d38b5452002"
                }
            ]
        },
        {
            "id": "a77345e9-75f0-4f14-9596-ea3ba1fc44ce",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7b14a0b7-c288-4de5-8bc8-3078d10d6945",
            "compositeImage": {
                "id": "f937ee1f-b99a-4571-89b7-e0d4502fcfb7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a77345e9-75f0-4f14-9596-ea3ba1fc44ce",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f43911c9-fe4f-4361-b143-2e5a771b7070",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a77345e9-75f0-4f14-9596-ea3ba1fc44ce",
                    "LayerId": "742fc847-05b4-44a4-9b8c-2d38b5452002"
                }
            ]
        },
        {
            "id": "5a44f992-4a28-4c59-ba09-c556339da29d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7b14a0b7-c288-4de5-8bc8-3078d10d6945",
            "compositeImage": {
                "id": "5c48e200-9d59-4124-84c5-47f15ea80cb4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5a44f992-4a28-4c59-ba09-c556339da29d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e3fa2bab-eef8-4d59-a39b-d03d4b30850f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5a44f992-4a28-4c59-ba09-c556339da29d",
                    "LayerId": "742fc847-05b4-44a4-9b8c-2d38b5452002"
                }
            ]
        },
        {
            "id": "36227ded-a3fb-463e-917d-def444095ee3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7b14a0b7-c288-4de5-8bc8-3078d10d6945",
            "compositeImage": {
                "id": "06315125-6da6-40d2-ab96-403b45d76dc9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "36227ded-a3fb-463e-917d-def444095ee3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2bd962f4-cedd-4cd4-acb5-be58202d1909",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "36227ded-a3fb-463e-917d-def444095ee3",
                    "LayerId": "742fc847-05b4-44a4-9b8c-2d38b5452002"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "742fc847-05b4-44a4-9b8c-2d38b5452002",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7b14a0b7-c288-4de5-8bc8-3078d10d6945",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 6,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}