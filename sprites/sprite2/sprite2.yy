{
    "id": "04066a74-e646-4aa3-bbf0-d32f90c7c48e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b0dcd9c8-6653-4983-81a6-a27491f020c9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "04066a74-e646-4aa3-bbf0-d32f90c7c48e",
            "compositeImage": {
                "id": "c2f8e330-607e-40f8-9b73-0d4876eb1f94",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b0dcd9c8-6653-4983-81a6-a27491f020c9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "89eaac33-27e6-4e8d-91e2-31334c4c270d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b0dcd9c8-6653-4983-81a6-a27491f020c9",
                    "LayerId": "9c99495d-ff11-4907-a56d-ada49f8ce2f2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "9c99495d-ff11-4907-a56d-ada49f8ce2f2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "04066a74-e646-4aa3-bbf0-d32f90c7c48e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}