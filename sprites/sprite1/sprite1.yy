{
    "id": "42c5d2d5-425b-4c39-a6cd-b2acb15ac5f3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b97fb7bf-1321-4b33-868a-d1e0eeb7375d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "42c5d2d5-425b-4c39-a6cd-b2acb15ac5f3",
            "compositeImage": {
                "id": "fa41b991-6f60-46fe-857b-c3add163880b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b97fb7bf-1321-4b33-868a-d1e0eeb7375d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "897a12c2-5f65-4d99-bdcf-5ded3b3335de",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b97fb7bf-1321-4b33-868a-d1e0eeb7375d",
                    "LayerId": "a46b2247-d1e3-4bd8-aa98-a8c98d3a609f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "a46b2247-d1e3-4bd8-aa98-a8c98d3a609f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "42c5d2d5-425b-4c39-a6cd-b2acb15ac5f3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}