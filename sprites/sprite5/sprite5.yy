{
    "id": "e1f3d9b8-c672-4670-aeb7-9c41da1d1a71",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite5",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 9,
    "bbox_left": 0,
    "bbox_right": 9,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "948e410f-f60a-4c41-8090-6766a905519f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1f3d9b8-c672-4670-aeb7-9c41da1d1a71",
            "compositeImage": {
                "id": "5bf7c30e-53fc-425a-8504-e89bde8154f2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "948e410f-f60a-4c41-8090-6766a905519f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "048b0d94-410c-40cc-bf13-827a0140e30b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "948e410f-f60a-4c41-8090-6766a905519f",
                    "LayerId": "faadf2f3-5856-4f55-980d-b83b27150981"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "faadf2f3-5856-4f55-980d-b83b27150981",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e1f3d9b8-c672-4670-aeb7-9c41da1d1a71",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}