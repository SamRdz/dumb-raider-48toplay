{
    "id": "eb7c9619-a1a9-4e0f-88a8-67124e97ee28",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite0",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e5b47a30-776c-4e04-a369-e7fa4de953a9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eb7c9619-a1a9-4e0f-88a8-67124e97ee28",
            "compositeImage": {
                "id": "de0b05cc-5693-4934-9a81-5d0a318e13ed",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e5b47a30-776c-4e04-a369-e7fa4de953a9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2e1d7bb6-5bc5-4f11-9b94-7376943051ac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e5b47a30-776c-4e04-a369-e7fa4de953a9",
                    "LayerId": "328f4ede-bf8b-4a96-8385-e8e5e6ada138"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "328f4ede-bf8b-4a96-8385-e8e5e6ada138",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "eb7c9619-a1a9-4e0f-88a8-67124e97ee28",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}