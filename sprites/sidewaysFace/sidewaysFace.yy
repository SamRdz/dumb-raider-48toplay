{
    "id": "350e6e30-31f6-4af9-92a9-2acf585cf042",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sidewaysFace",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 0,
    "bbox_right": 67,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f7cabb88-3b0d-4e72-bdc5-2b5ebfca2a5b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "350e6e30-31f6-4af9-92a9-2acf585cf042",
            "compositeImage": {
                "id": "f541c2e9-cbb7-49e4-824e-fed8212f51b1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f7cabb88-3b0d-4e72-bdc5-2b5ebfca2a5b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "be28a8e4-76bc-472c-9fc0-56b91334383b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f7cabb88-3b0d-4e72-bdc5-2b5ebfca2a5b",
                    "LayerId": "b3799aa8-3115-4d5d-bd9d-71f79ca31a24"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "b3799aa8-3115-4d5d-bd9d-71f79ca31a24",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "350e6e30-31f6-4af9-92a9-2acf585cf042",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 68,
    "xorig": 4,
    "yorig": 127
}