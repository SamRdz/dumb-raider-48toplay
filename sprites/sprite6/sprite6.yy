{
    "id": "ab62f890-534f-4d23-a0d4-2d109cad0980",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite6",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 18,
    "bbox_right": 55,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e759d1fb-3a00-4cbe-8bbf-0c844f0fce72",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ab62f890-534f-4d23-a0d4-2d109cad0980",
            "compositeImage": {
                "id": "47af19cb-e0b9-40a8-b8ea-4964b95d8ed5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e759d1fb-3a00-4cbe-8bbf-0c844f0fce72",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "591909bb-dd44-488e-bbfd-3831fe234303",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e759d1fb-3a00-4cbe-8bbf-0c844f0fce72",
                    "LayerId": "1b47691d-bc38-45c9-8909-7e0759e4dfa0"
                }
            ]
        },
        {
            "id": "26ace9a9-214b-4b5e-9731-ad98caa7c0cf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ab62f890-534f-4d23-a0d4-2d109cad0980",
            "compositeImage": {
                "id": "7291724a-122a-46ec-b8e1-ae1a27833cf1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "26ace9a9-214b-4b5e-9731-ad98caa7c0cf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "23389d75-ce11-41b5-b4ae-84bd468e46ab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "26ace9a9-214b-4b5e-9731-ad98caa7c0cf",
                    "LayerId": "1b47691d-bc38-45c9-8909-7e0759e4dfa0"
                }
            ]
        },
        {
            "id": "8a4c6561-8166-46b2-b320-64e4dcdc9e3d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ab62f890-534f-4d23-a0d4-2d109cad0980",
            "compositeImage": {
                "id": "69a46a4b-ac86-4acc-b6e1-d467964f4d13",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8a4c6561-8166-46b2-b320-64e4dcdc9e3d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4c5afe61-d56c-4c1e-a68f-4626a2260c53",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8a4c6561-8166-46b2-b320-64e4dcdc9e3d",
                    "LayerId": "1b47691d-bc38-45c9-8909-7e0759e4dfa0"
                }
            ]
        },
        {
            "id": "9d55877b-b1f9-49e5-ade4-37675a600fee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ab62f890-534f-4d23-a0d4-2d109cad0980",
            "compositeImage": {
                "id": "768ca553-a6d3-4815-a51d-6b90368d271a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9d55877b-b1f9-49e5-ade4-37675a600fee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5096f53c-8591-4553-92e5-9232be5d022b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9d55877b-b1f9-49e5-ade4-37675a600fee",
                    "LayerId": "1b47691d-bc38-45c9-8909-7e0759e4dfa0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "1b47691d-bc38-45c9-8909-7e0759e4dfa0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ab62f890-534f-4d23-a0d4-2d109cad0980",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}