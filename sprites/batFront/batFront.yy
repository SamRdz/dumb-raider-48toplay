{
    "id": "5d47fea8-9254-4fc8-bd90-dcf58eb81347",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "batFront",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d1793c6e-4158-4f6b-a147-42f970808db2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5d47fea8-9254-4fc8-bd90-dcf58eb81347",
            "compositeImage": {
                "id": "f9fa02ad-fb12-436d-ae9c-aced2978943f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d1793c6e-4158-4f6b-a147-42f970808db2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "47a2c4b4-0819-4b13-b4e9-24be31731788",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d1793c6e-4158-4f6b-a147-42f970808db2",
                    "LayerId": "373c9c59-91fa-44ec-97ba-e00c18657242"
                }
            ]
        },
        {
            "id": "85179f01-683f-4e16-a9fe-820bf2d70086",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5d47fea8-9254-4fc8-bd90-dcf58eb81347",
            "compositeImage": {
                "id": "63bc0586-c7f3-480c-a466-cd9d13f83230",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "85179f01-683f-4e16-a9fe-820bf2d70086",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "67029386-d82b-4034-9fbe-e97d4b7401b1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "85179f01-683f-4e16-a9fe-820bf2d70086",
                    "LayerId": "373c9c59-91fa-44ec-97ba-e00c18657242"
                }
            ]
        },
        {
            "id": "ae23e035-0b4e-4a52-9672-c08e13dc75f9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5d47fea8-9254-4fc8-bd90-dcf58eb81347",
            "compositeImage": {
                "id": "4e9db7eb-fe0d-416e-a7f2-a4bae17e1992",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ae23e035-0b4e-4a52-9672-c08e13dc75f9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aaed5393-1708-4fd6-9cc2-92dea41321d1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ae23e035-0b4e-4a52-9672-c08e13dc75f9",
                    "LayerId": "373c9c59-91fa-44ec-97ba-e00c18657242"
                }
            ]
        },
        {
            "id": "b1304f15-32fd-48b6-ab0e-327d436a7bf2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5d47fea8-9254-4fc8-bd90-dcf58eb81347",
            "compositeImage": {
                "id": "ad732518-3f72-46df-987c-f898bc022637",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b1304f15-32fd-48b6-ab0e-327d436a7bf2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "63c15309-9d57-4afd-bf86-f3b8a1605c9b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b1304f15-32fd-48b6-ab0e-327d436a7bf2",
                    "LayerId": "373c9c59-91fa-44ec-97ba-e00c18657242"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "373c9c59-91fa-44ec-97ba-e00c18657242",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5d47fea8-9254-4fc8-bd90-dcf58eb81347",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 8,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}