{
    "id": "05555592-286c-4803-9043-a1400d4b55bb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "brokenBlockLayer3",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0578f6f3-fec1-49a8-ae7b-f2f7f68fc9de",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "05555592-286c-4803-9043-a1400d4b55bb",
            "compositeImage": {
                "id": "eb7749db-71b9-4401-82e6-2ce0ff3db172",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0578f6f3-fec1-49a8-ae7b-f2f7f68fc9de",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a10f439f-404a-4986-94fd-212107fafd81",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0578f6f3-fec1-49a8-ae7b-f2f7f68fc9de",
                    "LayerId": "a971a96d-702b-4490-b109-0dd3c38253b1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "a971a96d-702b-4490-b109-0dd3c38253b1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "05555592-286c-4803-9043-a1400d4b55bb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}