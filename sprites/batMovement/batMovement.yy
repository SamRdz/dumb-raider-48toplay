{
    "id": "ede74a73-3e4c-4e44-9a1c-c4ffcdf3835c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "batMovement",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "de2367ba-361d-42d9-aa19-b3258426eae7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ede74a73-3e4c-4e44-9a1c-c4ffcdf3835c",
            "compositeImage": {
                "id": "6d01cfaa-d793-4ebb-b02d-568b8493973d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "de2367ba-361d-42d9-aa19-b3258426eae7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b91fb9c4-b32b-41d4-881a-ccc69562ac31",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "de2367ba-361d-42d9-aa19-b3258426eae7",
                    "LayerId": "f80d2ee6-cb37-4466-a1bc-56d06fe6364e"
                }
            ]
        },
        {
            "id": "0bda362e-49b9-4e00-b674-2f27d65929cf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ede74a73-3e4c-4e44-9a1c-c4ffcdf3835c",
            "compositeImage": {
                "id": "4759f7d5-7218-4b7b-b952-d40eb1f5a016",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0bda362e-49b9-4e00-b674-2f27d65929cf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4438ee8a-14d9-46d5-a70e-7cfc8d83102e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0bda362e-49b9-4e00-b674-2f27d65929cf",
                    "LayerId": "f80d2ee6-cb37-4466-a1bc-56d06fe6364e"
                }
            ]
        },
        {
            "id": "9e8febf8-cfb3-4c76-b31b-8dd3d879c426",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ede74a73-3e4c-4e44-9a1c-c4ffcdf3835c",
            "compositeImage": {
                "id": "ed0b9425-2b4d-4dea-891c-2b9718a54d33",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9e8febf8-cfb3-4c76-b31b-8dd3d879c426",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5a526392-dbe2-4b09-acdb-31b747df7de2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9e8febf8-cfb3-4c76-b31b-8dd3d879c426",
                    "LayerId": "f80d2ee6-cb37-4466-a1bc-56d06fe6364e"
                }
            ]
        },
        {
            "id": "750e5d9e-9404-4256-81ae-24af8b4c98f2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ede74a73-3e4c-4e44-9a1c-c4ffcdf3835c",
            "compositeImage": {
                "id": "3207bd95-043b-49fb-a06f-e34ac58ff23c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "750e5d9e-9404-4256-81ae-24af8b4c98f2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7f80348e-6560-42c4-888c-3e4074a64a16",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "750e5d9e-9404-4256-81ae-24af8b4c98f2",
                    "LayerId": "f80d2ee6-cb37-4466-a1bc-56d06fe6364e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "f80d2ee6-cb37-4466-a1bc-56d06fe6364e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ede74a73-3e4c-4e44-9a1c-c4ffcdf3835c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 8,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}