{
    "id": "c191cdb6-8e95-4dbf-bc61-ee6f32fc4f55",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "ballMoving",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 0,
    "bbox_right": 127,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7b96704b-3288-4bef-b541-09c87302ed66",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c191cdb6-8e95-4dbf-bc61-ee6f32fc4f55",
            "compositeImage": {
                "id": "93ae5877-2cf4-4907-aad8-636713740374",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7b96704b-3288-4bef-b541-09c87302ed66",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9436c165-bde4-480f-be89-64791e300b3e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7b96704b-3288-4bef-b541-09c87302ed66",
                    "LayerId": "8eda65d0-fe81-495f-b278-97bcd98e1fce"
                }
            ]
        },
        {
            "id": "c3c7cb3e-e542-4cd7-9f69-6c2807b06f52",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c191cdb6-8e95-4dbf-bc61-ee6f32fc4f55",
            "compositeImage": {
                "id": "e864f9f1-a30a-4ff9-a9de-e924426b44bf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c3c7cb3e-e542-4cd7-9f69-6c2807b06f52",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2740891a-8209-4344-866e-7572d1f5f8cf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c3c7cb3e-e542-4cd7-9f69-6c2807b06f52",
                    "LayerId": "8eda65d0-fe81-495f-b278-97bcd98e1fce"
                }
            ]
        },
        {
            "id": "72e68da6-4358-4e3d-86b0-0554daa96ba7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c191cdb6-8e95-4dbf-bc61-ee6f32fc4f55",
            "compositeImage": {
                "id": "396a3e2a-c1fa-4a3c-963e-ce014cedf151",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "72e68da6-4358-4e3d-86b0-0554daa96ba7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bc12a43c-37ee-4f44-80d3-d2c247b83ac9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "72e68da6-4358-4e3d-86b0-0554daa96ba7",
                    "LayerId": "8eda65d0-fe81-495f-b278-97bcd98e1fce"
                }
            ]
        },
        {
            "id": "75834c7d-a0b9-4eec-ab53-4194c9a6be29",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c191cdb6-8e95-4dbf-bc61-ee6f32fc4f55",
            "compositeImage": {
                "id": "79422626-4534-48f8-953a-4e1e3bb733ad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "75834c7d-a0b9-4eec-ab53-4194c9a6be29",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "81ea0536-4d5b-4531-a8fe-63b4e2cfb98a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "75834c7d-a0b9-4eec-ab53-4194c9a6be29",
                    "LayerId": "8eda65d0-fe81-495f-b278-97bcd98e1fce"
                }
            ]
        },
        {
            "id": "6f6051ef-894b-4e4b-b680-bcab3acad4c2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c191cdb6-8e95-4dbf-bc61-ee6f32fc4f55",
            "compositeImage": {
                "id": "00fbe01e-8b08-484c-b304-c115f307e806",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6f6051ef-894b-4e4b-b680-bcab3acad4c2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6e84ca48-8414-4740-969b-60b554163cb7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6f6051ef-894b-4e4b-b680-bcab3acad4c2",
                    "LayerId": "8eda65d0-fe81-495f-b278-97bcd98e1fce"
                }
            ]
        },
        {
            "id": "03e5c8cf-4397-47b4-8d6a-45f479fb792e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c191cdb6-8e95-4dbf-bc61-ee6f32fc4f55",
            "compositeImage": {
                "id": "093882d2-f2c5-4510-8da0-7645e68f892a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "03e5c8cf-4397-47b4-8d6a-45f479fb792e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e088f772-cfd6-4ce0-8120-cfec32e82dc3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "03e5c8cf-4397-47b4-8d6a-45f479fb792e",
                    "LayerId": "8eda65d0-fe81-495f-b278-97bcd98e1fce"
                }
            ]
        },
        {
            "id": "47d55c74-9e96-4187-9cfc-6f93610c709d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c191cdb6-8e95-4dbf-bc61-ee6f32fc4f55",
            "compositeImage": {
                "id": "b6bd9f24-e80f-4447-829e-22fafafa2573",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "47d55c74-9e96-4187-9cfc-6f93610c709d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5a7727fb-e499-4c22-b1ba-e0077b4b6fc7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "47d55c74-9e96-4187-9cfc-6f93610c709d",
                    "LayerId": "8eda65d0-fe81-495f-b278-97bcd98e1fce"
                }
            ]
        },
        {
            "id": "76bf2d71-37ab-47cb-ac27-fb40bbea6886",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c191cdb6-8e95-4dbf-bc61-ee6f32fc4f55",
            "compositeImage": {
                "id": "f1f266ff-23e2-44fb-a7eb-9d652fd08258",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "76bf2d71-37ab-47cb-ac27-fb40bbea6886",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1c921a77-e45d-41c7-82ed-2a0cd1b8eb64",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "76bf2d71-37ab-47cb-ac27-fb40bbea6886",
                    "LayerId": "8eda65d0-fe81-495f-b278-97bcd98e1fce"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "8eda65d0-fe81-495f-b278-97bcd98e1fce",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c191cdb6-8e95-4dbf-bc61-ee6f32fc4f55",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 0,
    "yorig": 0
}