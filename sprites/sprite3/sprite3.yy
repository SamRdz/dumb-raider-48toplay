{
    "id": "f127fce5-a9d4-4dd4-81ee-22e999c4aae7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite3",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ed895a2e-3fa3-4137-9b13-0e0c5248c51e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f127fce5-a9d4-4dd4-81ee-22e999c4aae7",
            "compositeImage": {
                "id": "b7827737-ef54-4968-b0c5-fa51a316afa2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ed895a2e-3fa3-4137-9b13-0e0c5248c51e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "579a8287-8b77-4532-a8be-146acb449e19",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ed895a2e-3fa3-4137-9b13-0e0c5248c51e",
                    "LayerId": "ae0e0a14-7563-4aaf-9849-5555d3ba6f68"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "ae0e0a14-7563-4aaf-9849-5555d3ba6f68",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f127fce5-a9d4-4dd4-81ee-22e999c4aae7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}