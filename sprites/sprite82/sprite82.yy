{
    "id": "6c8b1310-bee9-48af-9a68-eb9e9f739206",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite82",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1bde7970-accc-43fe-9162-ca9e949602ce",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6c8b1310-bee9-48af-9a68-eb9e9f739206",
            "compositeImage": {
                "id": "430a0729-548d-451b-b25f-d32b847c6a0d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1bde7970-accc-43fe-9162-ca9e949602ce",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "77bfdd60-6fa5-4fb2-90d0-dbe222cc2d0d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1bde7970-accc-43fe-9162-ca9e949602ce",
                    "LayerId": "3c9f0714-09f8-4d60-9205-c208788e8f69"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "3c9f0714-09f8-4d60-9205-c208788e8f69",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6c8b1310-bee9-48af-9a68-eb9e9f739206",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}