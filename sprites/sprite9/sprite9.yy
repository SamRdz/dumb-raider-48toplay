{
    "id": "8afcab62-efc8-4ec5-82aa-09840869e4b1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite9",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "114b7ce3-5b39-4dde-a26f-bcbe01a96a60",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8afcab62-efc8-4ec5-82aa-09840869e4b1",
            "compositeImage": {
                "id": "0f9386b2-c382-4f86-a5c9-43683830c482",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "114b7ce3-5b39-4dde-a26f-bcbe01a96a60",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ed911fde-e28b-4586-a82a-51b2db805fb3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "114b7ce3-5b39-4dde-a26f-bcbe01a96a60",
                    "LayerId": "fc5a7d57-b442-4839-9aa0-5b715490d71f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "fc5a7d57-b442-4839-9aa0-5b715490d71f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8afcab62-efc8-4ec5-82aa-09840869e4b1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}