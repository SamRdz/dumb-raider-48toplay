{
    "id": "5ffe4ca4-bd01-49dc-94e1-1eba41d62976",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "pressurePlatePressed",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 60,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cca8eda2-7bda-434a-a3ff-8bae335ebca9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5ffe4ca4-bd01-49dc-94e1-1eba41d62976",
            "compositeImage": {
                "id": "9156a938-6868-49d7-8722-4838589e2e0c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cca8eda2-7bda-434a-a3ff-8bae335ebca9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "64812734-d38d-4ed8-a739-7665fda740dd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cca8eda2-7bda-434a-a3ff-8bae335ebca9",
                    "LayerId": "b4254546-33ae-4865-b4cf-050a846562e9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "b4254546-33ae-4865-b4cf-050a846562e9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5ffe4ca4-bd01-49dc-94e1-1eba41d62976",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}