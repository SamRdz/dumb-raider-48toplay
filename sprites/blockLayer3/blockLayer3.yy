{
    "id": "cd780345-b28f-4477-b074-4f7be66d29af",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "blockLayer3",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6636c170-a46b-4298-9180-29637c6655d5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cd780345-b28f-4477-b074-4f7be66d29af",
            "compositeImage": {
                "id": "de9b70aa-6790-4637-9c1f-518df6a1b181",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6636c170-a46b-4298-9180-29637c6655d5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5bfe5a3b-d443-4f86-b28c-8aa706c473ca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6636c170-a46b-4298-9180-29637c6655d5",
                    "LayerId": "786a935d-23cb-46a4-aef0-c571dca8865b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "786a935d-23cb-46a4-aef0-c571dca8865b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cd780345-b28f-4477-b074-4f7be66d29af",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}