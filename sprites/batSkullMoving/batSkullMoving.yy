{
    "id": "8750587c-edb9-4caa-9667-73ae3b075b6c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "batSkullMoving",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d6a0428d-0dec-4fd2-946a-69fefcbde495",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8750587c-edb9-4caa-9667-73ae3b075b6c",
            "compositeImage": {
                "id": "7793db23-9e83-40d9-9ff7-c5d0bbe6a3d4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d6a0428d-0dec-4fd2-946a-69fefcbde495",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fa586c90-531f-4fa6-bbbc-8dfc57a61340",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d6a0428d-0dec-4fd2-946a-69fefcbde495",
                    "LayerId": "2606120c-e41c-435a-813d-449ce3a3c206"
                }
            ]
        },
        {
            "id": "15f5d91d-1d55-4719-be22-077290f9eaab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8750587c-edb9-4caa-9667-73ae3b075b6c",
            "compositeImage": {
                "id": "76e63342-87fa-4d91-9405-91114fb2d0a5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "15f5d91d-1d55-4719-be22-077290f9eaab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "56b31397-5f68-4787-8a8a-2de5be08cbdb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "15f5d91d-1d55-4719-be22-077290f9eaab",
                    "LayerId": "2606120c-e41c-435a-813d-449ce3a3c206"
                }
            ]
        },
        {
            "id": "268f042d-1bad-40a3-9fa7-bec775dfd87f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8750587c-edb9-4caa-9667-73ae3b075b6c",
            "compositeImage": {
                "id": "b09bef79-9eb7-4fdf-b2a2-766fd7bdd9d4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "268f042d-1bad-40a3-9fa7-bec775dfd87f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5bc1ceef-2890-45b6-a3b4-bbb17e2ce149",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "268f042d-1bad-40a3-9fa7-bec775dfd87f",
                    "LayerId": "2606120c-e41c-435a-813d-449ce3a3c206"
                }
            ]
        },
        {
            "id": "c938db5f-f6c1-47ff-86ec-5af9f8c5c0f5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8750587c-edb9-4caa-9667-73ae3b075b6c",
            "compositeImage": {
                "id": "ce52c573-e124-4fec-9575-dc17e1959c56",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c938db5f-f6c1-47ff-86ec-5af9f8c5c0f5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b707b0e8-ec34-4086-9c07-4f27fe91661b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c938db5f-f6c1-47ff-86ec-5af9f8c5c0f5",
                    "LayerId": "2606120c-e41c-435a-813d-449ce3a3c206"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "2606120c-e41c-435a-813d-449ce3a3c206",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8750587c-edb9-4caa-9667-73ae3b075b6c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 8,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}