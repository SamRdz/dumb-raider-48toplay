{
    "id": "1c6ec521-f5e1-458a-ab70-ae29dff30b35",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "mossBlockLayer3",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "95e47fa8-f16e-4de5-9bd8-ca6f3c0459ef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1c6ec521-f5e1-458a-ab70-ae29dff30b35",
            "compositeImage": {
                "id": "6553543e-6f9d-48ca-bbf6-4786e068182a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "95e47fa8-f16e-4de5-9bd8-ca6f3c0459ef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "43ac8115-5b72-4fab-b16a-dbf19d2631c0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "95e47fa8-f16e-4de5-9bd8-ca6f3c0459ef",
                    "LayerId": "bed608bc-b8ef-4487-a03d-6a4292de977f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "bed608bc-b8ef-4487-a03d-6a4292de977f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1c6ec521-f5e1-458a-ab70-ae29dff30b35",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}