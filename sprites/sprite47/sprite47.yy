{
    "id": "2af4554d-488c-4370-bed1-1ca22a28c045",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite47",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5a30a1d8-0230-4318-840b-168fb74ae699",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2af4554d-488c-4370-bed1-1ca22a28c045",
            "compositeImage": {
                "id": "ab9a7229-a23f-4336-ad1b-69f4a8875742",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5a30a1d8-0230-4318-840b-168fb74ae699",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6b9bf132-9533-4593-8e5d-3c078feb99fe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5a30a1d8-0230-4318-840b-168fb74ae699",
                    "LayerId": "b7ef7711-bf01-486b-87f1-712010012eb7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "b7ef7711-bf01-486b-87f1-712010012eb7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2af4554d-488c-4370-bed1-1ca22a28c045",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}