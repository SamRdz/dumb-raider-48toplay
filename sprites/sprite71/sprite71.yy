{
    "id": "9fece221-617f-4872-94bb-2fd4e63c0368",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite71",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0298d20b-a47d-4688-a645-59df8bd39174",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9fece221-617f-4872-94bb-2fd4e63c0368",
            "compositeImage": {
                "id": "988aaf96-bbd6-4425-84d1-ae7c0e010fca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0298d20b-a47d-4688-a645-59df8bd39174",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "50874cef-3f55-4f5b-89fe-63c1f04f1aae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0298d20b-a47d-4688-a645-59df8bd39174",
                    "LayerId": "f35fa265-728c-426b-ab1c-9e59470d5620"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "f35fa265-728c-426b-ab1c-9e59470d5620",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9fece221-617f-4872-94bb-2fd4e63c0368",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}