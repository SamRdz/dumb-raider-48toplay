{
    "id": "787388cd-3b2c-4859-b8f2-756c1cc2e878",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite32",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4cce45c8-d09d-4c65-8aa6-e109d94cbf99",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "787388cd-3b2c-4859-b8f2-756c1cc2e878",
            "compositeImage": {
                "id": "48908988-12b0-4890-a96f-3808306e5c45",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4cce45c8-d09d-4c65-8aa6-e109d94cbf99",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "29a0c3cd-636c-4e95-89b6-3105050329f6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4cce45c8-d09d-4c65-8aa6-e109d94cbf99",
                    "LayerId": "98478fa5-fc3f-485b-810e-a14fda619819"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "98478fa5-fc3f-485b-810e-a14fda619819",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "787388cd-3b2c-4859-b8f2-756c1cc2e878",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}