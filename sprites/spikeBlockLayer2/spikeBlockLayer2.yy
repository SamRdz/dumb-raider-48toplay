{
    "id": "0bad8820-751f-40dd-91ec-38964a88ff5c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spikeBlockLayer2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 1,
    "bbox_right": 62,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "654cf4b7-c17a-46fb-951c-d179110eaf8c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0bad8820-751f-40dd-91ec-38964a88ff5c",
            "compositeImage": {
                "id": "ef724f0f-b2e0-434a-a6b6-439e4125304f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "654cf4b7-c17a-46fb-951c-d179110eaf8c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3c04bcf6-60ec-4a8a-b569-2e3013549e72",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "654cf4b7-c17a-46fb-951c-d179110eaf8c",
                    "LayerId": "842208d8-63a1-4939-bee6-64a5d2930fcb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "842208d8-63a1-4939-bee6-64a5d2930fcb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0bad8820-751f-40dd-91ec-38964a88ff5c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}