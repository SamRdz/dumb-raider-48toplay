{
    "id": "b84f7e5a-e176-46d6-8e88-88e4d41afb07",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "crackedBlockV1Layer2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "60bfbfbc-5af9-4ff9-847c-9a22afdeac77",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b84f7e5a-e176-46d6-8e88-88e4d41afb07",
            "compositeImage": {
                "id": "0b3f5985-33bf-4428-92c8-13793787e406",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "60bfbfbc-5af9-4ff9-847c-9a22afdeac77",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "84c1ca58-6b14-4f74-8a62-5ec9531e5f3a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "60bfbfbc-5af9-4ff9-847c-9a22afdeac77",
                    "LayerId": "8c0e6cb4-cf42-447a-91a2-c8b59f350287"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "8c0e6cb4-cf42-447a-91a2-c8b59f350287",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b84f7e5a-e176-46d6-8e88-88e4d41afb07",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}