{
    "id": "0f257fae-2f54-47fc-a116-0f8c3dac6c5b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "pillar1",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "c0a48bd2-3d97-4e11-8e4e-e4775716d25e",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "a61a7a4d-9659-4c70-a3a3-e9972be3af22",
    "visible": true
}