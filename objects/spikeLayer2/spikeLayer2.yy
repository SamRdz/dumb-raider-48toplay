{
    "id": "1e3ab6b9-b4d9-49e1-bd8b-27e04e8cedcf",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "spikeLayer2",
    "eventList": [
        {
            "id": "334ded2c-e6e4-42dc-b073-2ff3ab261f5f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "1e3ab6b9-b4d9-49e1-bd8b-27e04e8cedcf"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "c0a48bd2-3d97-4e11-8e4e-e4775716d25e",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "0bad8820-751f-40dd-91ec-38964a88ff5c",
    "visible": true
}