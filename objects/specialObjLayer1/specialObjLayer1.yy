{
    "id": "7da90f9c-65fa-463f-9cff-521a4ad4b968",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "specialObjLayer1",
    "eventList": [
        {
            "id": "a34f0ebe-87be-4019-aae7-07123da04276",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "7da90f9c-65fa-463f-9cff-521a4ad4b968"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "c0a48bd2-3d97-4e11-8e4e-e4775716d25e",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "c7e2040c-736b-4d09-a06c-ea4dbfd746ff",
    "visible": true
}