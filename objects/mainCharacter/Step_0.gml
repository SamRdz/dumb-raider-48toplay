/// @description Insert description here
// You can write your code in this editor
key_left = keyboard_check(vk_left);
key_right = keyboard_check(vk_right);
key_jump = keyboard_check(vk_space);

var move = key_right - key_left;
hsp = move * walkspd;
vsp += grv;

//region Jump Condition
if ((place_meeting(x, y + 1, blockParent)) && key_jump) 
{
	vsp = -7;
}
//endregion

//region Horizontal Collition
if (place_meeting((x + hsp), y, blockParent))
{
	while((!place_meeting((x + sign(hsp)), y, blockParent)))
	{
		x += sign(hsp);
	}
	hsp = 0;
}
x += hsp;
//endregion

//region Vertical Collition
if (place_meeting(x, (y + vsp), blockParent))
{
	while((!place_meeting(x, (y + sign(vsp)), blockParent)))
	{
		y += sign(vsp);
	}
	vsp = 0;
}
y += vsp;
//endregion