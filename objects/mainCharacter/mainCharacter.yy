{
    "id": "aff8fde3-a19b-4d25-9a3e-e87b5d184040",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "mainCharacter",
    "eventList": [
        {
            "id": "229af3ad-0c44-439e-af5f-36a432b75958",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "aff8fde3-a19b-4d25-9a3e-e87b5d184040"
        },
        {
            "id": "4edb4713-79f7-4566-bdd0-5e1df27d95ba",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "aff8fde3-a19b-4d25-9a3e-e87b5d184040"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "ab62f890-534f-4d23-a0d4-2d109cad0980",
    "visible": true
}