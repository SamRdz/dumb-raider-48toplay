{
    "id": "4aae8aac-2424-4124-a362-84aed1b79df5",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "specialObjLayer3",
    "eventList": [
        {
            "id": "d99cccc4-4c35-4490-818c-c12626615d8f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "4aae8aac-2424-4124-a362-84aed1b79df5"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "c0a48bd2-3d97-4e11-8e4e-e4775716d25e",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "6c8b1310-bee9-48af-9a68-eb9e9f739206",
    "visible": true
}