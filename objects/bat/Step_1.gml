/// @description Insert description here
// You can write your code in this editor
switch(state)
{
	case STATE_REST:
	{
		if(place_meeting(x, y, player))
		{
			state = STATE_HIGHLIGHT;
		}
		else
		{
			state = STATE_REST;
		}
		sprite_index = batRoof;
		break;
	}
	case STATE_HIGHLIGHT:
	{
		if(place_meeting(x, y, player))
		{
			state = STATE_HIGHLIGHT;
		}
		else
		{
			state = STATE_REST;
		}
		sprite_index = batRoofHighlight;
		break;
	}
	case STATE_AWAKEN:
	{
		sprite_index = batFront;
		break;
	}
	case STATE_MOVING:
	{
		sprite_index = batSkullMoving;
		break;
	}
	case STATE_TORCH:
	{
		sprite_index = batMovement;
		break;
	}
}