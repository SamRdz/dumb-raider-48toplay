{
    "id": "813001ad-4cc5-498c-bcb6-3b3fc3378e86",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "bat",
    "eventList": [
        {
            "id": "f32065f3-9307-4dbd-b6e0-5f18b3a47528",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "813001ad-4cc5-498c-bcb6-3b3fc3378e86"
        },
        {
            "id": "419649d7-2833-461c-a2e9-21b6bb00d731",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "813001ad-4cc5-498c-bcb6-3b3fc3378e86"
        },
        {
            "id": "c43e33c5-a22c-42ea-a1c4-8fa73f1f7f70",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "813001ad-4cc5-498c-bcb6-3b3fc3378e86"
        },
        {
            "id": "2599df1e-4672-4777-bbcb-b76e53f63d42",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "813001ad-4cc5-498c-bcb6-3b3fc3378e86"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "9f1d17d1-cf3d-4862-a48a-2153cbf1ac37",
    "visible": true
}