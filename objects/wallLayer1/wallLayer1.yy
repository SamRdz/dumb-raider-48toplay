{
    "id": "d06aa37d-df7b-49aa-a899-d61a8721f854",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "wallLayer1",
    "eventList": [
        {
            "id": "6e5360af-3389-42bd-b9ee-6a4b028be093",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "d06aa37d-df7b-49aa-a899-d61a8721f854"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "c0a48bd2-3d97-4e11-8e4e-e4775716d25e",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "787388cd-3b2c-4859-b8f2-756c1cc2e878",
    "visible": true
}