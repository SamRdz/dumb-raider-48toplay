{
    "id": "d8228a2f-ce19-40ec-84f9-5b89882e5b47",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "wallLayer2",
    "eventList": [
        {
            "id": "83e1a006-3280-4b78-b5b7-163493a91d31",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "d8228a2f-ce19-40ec-84f9-5b89882e5b47"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "c0a48bd2-3d97-4e11-8e4e-e4775716d25e",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "24a39e8d-64f6-428f-8e6f-e62f41152ac8",
    "visible": true
}