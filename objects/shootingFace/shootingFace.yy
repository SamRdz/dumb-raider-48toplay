{
    "id": "119b6be8-b418-48a3-b6d8-069f05cce32d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "shootingFace",
    "eventList": [
        {
            "id": "f2c69ffb-2315-4ce7-8b95-b36d3363844f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "119b6be8-b418-48a3-b6d8-069f05cce32d"
        },
        {
            "id": "850653a6-8b27-48a8-a4c0-9b3612c793f5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "119b6be8-b418-48a3-b6d8-069f05cce32d"
        },
        {
            "id": "87e757e3-6746-40e3-9766-6aa85e5dc5f8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "119b6be8-b418-48a3-b6d8-069f05cce32d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "350e6e30-31f6-4af9-92a9-2acf585cf042",
    "visible": true
}