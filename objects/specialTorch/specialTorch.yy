{
    "id": "4c7825c1-a0de-4026-80c1-3571a03c8068",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "specialTorch",
    "eventList": [
        {
            "id": "b23381c5-b356-43ba-92f1-5d93c2d12599",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "4c7825c1-a0de-4026-80c1-3571a03c8068"
        },
        {
            "id": "b105e047-12da-4b71-bcc7-afc7e633f7c7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "4c7825c1-a0de-4026-80c1-3571a03c8068"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "2199c2c0-85c7-4546-a25b-f23e1430581c",
    "visible": true
}