{
    "id": "d715f405-a8c0-403c-88bc-2b266e227d57",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "arrow",
    "eventList": [
        {
            "id": "4669ceb5-1619-4bd0-9ace-dcd476f32a5a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "d715f405-a8c0-403c-88bc-2b266e227d57"
        },
        {
            "id": "404bc9bd-2f2f-4385-80a5-586f89904d10",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "d715f405-a8c0-403c-88bc-2b266e227d57"
        },
        {
            "id": "d77e9783-4aec-4ba0-ab36-4f90b61ef4a4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "d715f405-a8c0-403c-88bc-2b266e227d57"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "bb5cc102-88d9-4cfb-b735-7ec6ac9097ba",
    "visible": true
}