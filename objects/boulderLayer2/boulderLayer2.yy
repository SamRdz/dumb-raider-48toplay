{
    "id": "922cf0ae-0443-440a-8aea-b14cab19cf10",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "boulderLayer2",
    "eventList": [
        {
            "id": "7d458bce-d4ff-4c48-832d-237439cf53e3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "922cf0ae-0443-440a-8aea-b14cab19cf10"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "c0a48bd2-3d97-4e11-8e4e-e4775716d25e",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "3f93faa0-0c20-4f62-9d96-f9c3b96b817d",
    "visible": true
}