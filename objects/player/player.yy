{
    "id": "6d2310d6-35fb-496e-b303-5aa4852c7145",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "player",
    "eventList": [
        {
            "id": "42a680bb-91cf-4954-9a25-a2fce533c5e7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "6d2310d6-35fb-496e-b303-5aa4852c7145"
        },
        {
            "id": "756fade0-9529-4bdf-9d7e-9b07bdf11f2b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "6d2310d6-35fb-496e-b303-5aa4852c7145"
        },
        {
            "id": "3ef7db7d-36fa-4215-a4b6-372f041603b5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 38,
            "eventtype": 9,
            "m_owner": "6d2310d6-35fb-496e-b303-5aa4852c7145"
        },
        {
            "id": "281f2202-a71c-4b24-9aea-bf2fbf3d9bba",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 40,
            "eventtype": 9,
            "m_owner": "6d2310d6-35fb-496e-b303-5aa4852c7145"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "e1f3d9b8-c672-4670-aeb7-9c41da1d1a71",
    "visible": true
}