{
    "id": "88c96fe4-f136-4e8e-b7b1-31b6bdedc00f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "brokenWallLayer2",
    "eventList": [
        {
            "id": "fcbadd3d-bde9-4521-bf45-cabdd343c07c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "88c96fe4-f136-4e8e-b7b1-31b6bdedc00f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "c0a48bd2-3d97-4e11-8e4e-e4775716d25e",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "9d8cbff4-1fed-427b-8461-d99076aca274",
    "visible": true
}