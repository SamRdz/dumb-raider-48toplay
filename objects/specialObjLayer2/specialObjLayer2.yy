{
    "id": "3834d141-b618-41e6-92e1-5fecf50d1da8",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "specialObjLayer2",
    "eventList": [
        {
            "id": "c145c04d-5fd8-4601-81b3-c0cf8ec4bfe5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "3834d141-b618-41e6-92e1-5fecf50d1da8"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "c0a48bd2-3d97-4e11-8e4e-e4775716d25e",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "b84f7e5a-e176-46d6-8e88-88e4d41afb07",
    "visible": true
}