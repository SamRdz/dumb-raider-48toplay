{
    "id": "45bdd0fc-ff74-4134-b308-a3187ba73c50",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "spikeLayer3",
    "eventList": [
        {
            "id": "e202ee21-9b01-4324-a2b1-9eb8315371e5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "45bdd0fc-ff74-4134-b308-a3187ba73c50"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "c0a48bd2-3d97-4e11-8e4e-e4775716d25e",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "0bad8820-751f-40dd-91ec-38964a88ff5c",
    "visible": true
}